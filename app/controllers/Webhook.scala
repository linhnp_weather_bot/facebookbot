package controllers

import javax.inject.Inject
import play.api.{Configuration, Logger}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class Webhook @Inject()(ws: WSClient, cp: ControllerComponents, config: Configuration) extends AbstractController(cp) {
  val verifyToken: String = sys.env.getOrElse("PAGE_ACCESS_TOKEN", config.get[String]("BACKUP_TOKEN"))
  val logger: Logger = Logger(this.getClass())

  // for subscribe
  def get() = Action { request =>

    val mode: String = request.getQueryString("hub.mode").getOrElse("")
    val verify_token = request.getQueryString("hub.verify_token").getOrElse("")
    val challenge = request.getQueryString("hub.challenge").getOrElse("")

    if (!mode.isEmpty && !verify_token.isEmpty) {
      if (mode == "subscribe" && verify_token == verifyToken) {
        print("verify")
        Ok(challenge)
      } else {
        Forbidden
      }
    } else {
      Forbidden
    }
  }

  def post() = Action { request =>
    if (request.hasBody) {
      val body = request.body.asJson.get

      if ((body \ "object").as[String] == "page") {
        for (entry <- (body \ "entry").as[List[JsValue]]) {
          val event = (entry \ "messaging") (0)

          val sender_psid = (event \ "sender" \ "id").as[String]

          if ((event \ "message").isDefined) {
            // handleMessage
            handleMessage(event, sender_psid)
          }
        }
        Ok
      } else {
        NotFound
      }
    } else {
      NotFound
    }
  }

  def handleMessage(event: JsValue, sender_psid: String): Unit = {
    var response_text = ""
    if ((event \ "message" \ "text").isDefined) {
      val text = (event \ "message" \ "text").as[String]

      val url = "https://nameless-falls-36254.herokuapp.com/api/weather"

      val request: Future[WSResponse] = ws.url(url)
        .withQueryStringParameters("q" -> text).get

      request onComplete {
        case Success(t) =>
          response_text = processResponse(t.json)

          // sendAPI
          callSendAPI(sender_psid, response_text)

        case Failure(t) => print("Error: " + t.getMessage)
      }
    }
  }

  def processResponse(json: JsValue): String = {
    val message = (json \ "message").as[String]
    val min_temp = if ((json \ "min_temp").isDefined) (json \ "min_temp").as[BigDecimal].toString else ""
    val max_temp = if ((json \ "max_temp").isDefined) (json \ "max_temp").as[BigDecimal].toString else ""
    val desc = if ((json \ "desc").isDefined) (json \ "desc").as[String] else ""

    if (min_temp.isEmpty) {
      message
    } else {
      if (max_temp.isEmpty) {
        f"$message\nNhiệt độ: $min_temp\n$desc"
      } else {
        f"$message\nNhiệt độ: $min_temp ~ $max_temp\n$desc"
      }
    }
  }

  def callSendAPI(sender_psid: String, response_text: String): Unit = {
    val url = "https://graph.facebook.com/v2.6/me/messages"
    val body = Json.obj(
      "recipient" -> Json.obj(
        "id" -> sender_psid
      ),
      "message" -> Json.obj(
        "text" -> response_text
      )
    )

    val request: Future[WSResponse] = ws.url(url)
      .withQueryStringParameters("access_token" -> verifyToken).post(body)

    request onComplete {
      case Success(t) => println("Success")
      case Failure(t) => println("Error: " + t.getMessage)
    }
  }
}
