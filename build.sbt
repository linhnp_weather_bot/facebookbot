name := "facebookweatherbot"

version := "0.1"

scalaVersion := "2.12.4"

lazy val amin = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  guice,
  ws
)